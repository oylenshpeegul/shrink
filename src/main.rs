use image::GenericImageView;
use std::path::{Path, PathBuf};
use std::thread;
use structopt::StructOpt;

/// Shrink options
#[derive(StructOpt, Debug)]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Input file or directory
    #[structopt(parse(from_os_str), default_value = ".")]
    input: PathBuf,

    /// Output file or directory.
    ///
    /// If the input is a directory "foo", this will default to
    /// "foo/shrink". If the input is a file "foo.ext", this will
    /// default to "foo.shrink.ext".
    #[structopt(parse(from_os_str))]
    output: Option<PathBuf>,

    /// Size to make image
    #[structopt(short, long, default_value = "800")]
    size: u32,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,
}

fn main() {
    let opt = Opt::from_args();
    let size = opt.size;

    // Echo options if the debug flag was set.
    if opt.debug {
        eprintln!("{:#?}", opt);
    }

    let output_path = get_output_path(&opt);

    let image_files = match get_image_files(&opt) {
        Ok(paths) => paths,
        Err(e) => {
            panic!("Unable to get image files {:#?}: {}", opt.input, e);
        }
    };

    let mut handles = vec![];
    for path in image_files {
        if opt.verbose > 0 {
            println!("image file: {:#?}", path);
        }
        if let Some(base_name) = path.file_name() {
            let mut dest = PathBuf::from(&output_path);
            if !opt.input.is_file() {
                dest.push(base_name);
            }
            let handle = thread::spawn(move || resize_for_web(&path, dest, size));
            handles.push(handle);
        }
    }
    for handle in handles {
        match handle.join().unwrap() {
            Ok(status) => {
                if opt.verbose > 0 {
                    println!("\n{}", status);
                }
            }
            Err(e) => {
                eprintln!("\n{}", e);
            }
        }
    }
}

/// Get a list of all files of type GIF, JPEG, or PNG.
///
/// If we're given just a file name, return it in a vector. If we're
/// given a directory, return a vector of all the image files in that
/// directory.
fn get_image_files(opt: &Opt) -> std::io::Result<Vec<PathBuf>> {
    let mut f = vec![];

    if opt.input.is_file() {
        f.push(opt.input.clone());
    } else {
        for entry in opt.input.read_dir()? {
            if opt.debug {
                println!("entry {:?}", entry);
            }
            let p = entry?.path();
            if let Some(ext) = p.extension().and_then(std::ffi::OsStr::to_str) {
                match ext {
                    "gif" | "jpg" | "png" => f.push(p),
                    _ => {}
                }
            }
        }
    }

    Ok(f)
}

fn get_output_path(opt: &Opt) -> PathBuf {
    // If we're given an output path, use it. Otherwise, make one from
    // the input path.
    let output_path = match &opt.output {
        Some(path) => path.clone(),
        None => {
            let mut p = PathBuf::from(&opt.input);
            if opt.input.is_file() {
                if let Some(ext) = p.extension().and_then(std::ffi::OsStr::to_str) {
                    let mut s = "shrink.".to_owned();
                    s.push_str(ext);
                    p = p.with_extension(s);
                }
            } else {
                p.push("shrink");
            }
            p
        }
    };
    if opt.debug {
        println!("output path: {:#?}", output_path);
    }

    // Create the output directory if it does not exist already.
    if !opt.input.is_file() {
        if output_path.is_dir() {
            if opt.verbose > 0 {
                println!("Output path '{}' exists", output_path.display());
            }
        } else {
            let result = std::fs::create_dir(&output_path);
            match result {
                Ok(()) => {
                    if opt.verbose > 0 {
                        println!("Created output path '{}'", output_path.display());
                    }
                }
                Err(e) => {
                    eprintln!("Cannot create directory {:#?}: {}", output_path, e);
                    std::process::exit(1);
                }
            }
        }
    }

    output_path
}

/// Resize the given image for the web and save it to the given destination.
fn resize_for_web(source: &Path, destination: PathBuf, size: u32) -> Result<String, String> {
    let img = match image::open(source) {
        Ok(image) => image,
        Err(e) => {
            return Err(format!("Cannot open image {:#?}: {}", source, e));
        }
    };

    // https://www.impulseadventure.com/photo/exif-orientation.html
    let img = match get_orientation(source) {
        Some(3) => img.rotate180(),
        Some(6) => img.rotate90(),
        Some(8) => img.rotate270(),
        _ => img,
    };

    let web_img = img.resize(size, size, image::FilterType::Lanczos3);
    match web_img.save(&destination) {
        Ok(()) => Ok(format!(
            "source: {:?}: {:?}\ndestination: {:?}:  {:?}",
            source,
            img.dimensions(),
            &destination,
            web_img.dimensions()
        )),
        Err(e) => Err(format!("Cannot save image {:#?}: {}", &destination, e)),
    }
}

/// Get the orientation from the EXIF data if it's there.
fn get_orientation(source: &Path) -> Option<u32> {
    let file = match std::fs::File::open(source) {
        Err(e) => {
            eprintln!("Cannot open file '{:?}': {}", source, e);
            return None;
        }
        Ok(f) => f,
    };

    let reader = match exif::Reader::new(&mut std::io::BufReader::new(&file)) {
        Err(e) => {
            eprintln!("Cannot read metadata in file '{:?}': {}", source, e);
            return None;
        }
        Ok(r) => r,
    };

    reader
        .get_field(exif::Tag::Orientation, exif::In::PRIMARY)?
        .value
        .get_uint(0)
}
