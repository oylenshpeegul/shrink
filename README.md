# shrink

Shrink images for the web.

My camera takes fairly high resolution images and I often want to
shrink them down to share on the web. This shrinks all the images in a
directory using Rust's [image](https://crates.io/crates/image)
library.

```
$ shrink --help
shrink 0.1.0
Shrink options

USAGE:
    shrink [FLAGS] [OPTIONS] [ARGS]

FLAGS:
    -d, --debug      Activate debug mode
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Verbose mode (-v, -vv, -vvv, etc.)

OPTIONS:
    -s, --size <size>    Size to make image [default: 800]

ARGS:
    <input>     Input file or directory [default: .]
    <output>    Output file or directory
    
                If the input is a directory "foo", this will default to "foo/shrink". If the input is a file "foo.ext", this
                will default to "foo.shrink.ext".

```

If our current working directory had these files in it

```
$ ls -l
total 216
-rw-r--r-- 1 tim tim      0 Nov 30 17:54 foo.bar
---------- 1 tim tim      0 Nov 30 17:54 foo.jpg
-rw-r--r-- 1 tim tim 165666 Dec  6 15:35 NotLDeb.jpg
-rw-r--r-- 1 tim tim  52253 Nov 30 10:43 porygon2.png
```

Then running `shrink` in verbose mode would look like this

```
$ shrink -v
Created output path './shrink'
image file: "./porygon2.png"
image file: "./foo.jpg"
image file: "./NotLDeb.jpg"

source: "./porygon2.png": (326, 548)
destination: "./shrink/porygon2.png":  (475, 800)

Cannot open image "./foo.jpg": Permission denied (os error 13)

source: "./NotLDeb.jpg": (1024, 630)
destination: "./shrink/NotLDeb.jpg":  (800, 492)

$ tree shrink
shrink
├── NotLDeb.jpg
└── porygon2.png

0 directories, 2 files
```

It created a "shrink" directory and put the new files in
there. Alternatively, you can name an output directory and it will put
the files in there. You can also give it a single file and it will add
a ".shrink" before the file extension. Alternatively, you can name an
output file and it will put it there.

```
$ shrink NotLDeb.jpg
$ ls -l
-rw-r--r-- 1 tim tim 165666 Dec  6 15:35 NotLDeb.jpg
-rw-r--r-- 1 tim tim  71416 Dec  6 17:53 NotLDeb.shrink.jpg
```
